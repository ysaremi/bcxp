# BettercallPaul Programming Challenge
*Author: Yasmin Saremi, yasmin.saremi@gmail.com*

This is my solution to the BetterCall Porgramming Challenge (https://github.com/bettercodepaul/programming-challenge). It was designed as a Java Programme but in this instance I will be solving the tasks using Python 3.7.16.

## The Task

*Weather*  
In **weather.csv** you’ll find the daily weather data of a single month. Read the file, then output the day number (column one Day) of the day with the smallest temperature spread (difference between maximum & minimum temperature of the day.) The maximum temperature is the second column MxT, the minimum temperature the third column MnT.

Countries  
**countries.csv** contains a list of data about the 27 countries of the European Union. Use the columns Population and Area to compute each country’s population density. Read the file, then print the name of the country with the highest number of people per square kilometre.

Task  
- Write a single program which is able to solve the "Weather" challenge.

- Then refactor and extend your solution such that it additionally supports the Countries challenge.

Process
- Favour the software design goals described under Goal over other goals like performance or feature set.

- Include unit tests to ensure the correctness of your implementation.

## The Goal
Implement a solution for the given tasks which aims for

- robustness & correctness

- readability & maintainability

- clean software design & architecture

## Notes To The Solution
I wrote the unit tests in a Jupyter notebook (test.ipynb) to ensure that the CSV input files were not corrupted. These tests revealed that **countries.csv** has inconsistent use of delimiters and does not use commas as delimiters. This is a problem when converting the csv file to a Pandas data frame. Therefore, I fixed this problem in the main.ipynb file before solving the tasks.